<?php

/**
 * @file Implementation of the User name highlighter module.
 *
 * Provides an input filter that adds a unique background color to user names
 * in the filtered text.
 */

/**
 * Implementation of hook_menu().
 */
function username_highlighter_menu() {
  $items = array();
  $items['admin/settings/username_highlighter'] = array(
    'title' => 'User name highlighter',
    'description' => 'Administer user name highlighting.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('username_highlighter_admin'),
    'access arguments' => array('administer filters'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}


/**
 * Implementation of hook_filter().
 */
function username_highlighter_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {

    case 'list':
      return array(0 => t('User name highlighter'));

    case 'description':
      return t('Highlights each user name with a unique color.');

    case 'settings':
      return NULL;

    case 'prepare':
      return $text;

    case 'process':
      if ($delta == 0) {
        $username_map = _username_highlighter_username_map();
        return preg_replace(array_keys($username_map), array_values($username_map), $text);
      }

    default:
      return $text;
  }
}

/**
 * Implementation of hook_filter_tips().
 */
function username_highlighter_filter_tips($delta, $format, $long = FALSE) {
  return t('User names will be highlighted.');
}

/**
 * Administration form builder function.
 */
function username_highlighter_admin() {
  $form = array();

  // Widget for setting the color saturation.
  $form['username_highlighter_color_saturation'] = array(
    '#type' => 'textfield',
    '#title' => t('Color saturation'),
    '#description' => t('The color saturation of the highlighting.'),
    '#default_value' => variable_get('username_highlighter_color_saturation', 50),
    '#size' => 4,
    '#maxlength' => 3,
    '#field_suffix' => '%',
  );
  // Widget for setting the color value.
  $form['username_highlighter_color_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Color value'),
    '#description' => t('The color value of the highlighting, lower means darker.'),
    '#default_value' => variable_get('username_highlighter_color_value', 100),
    '#size' => 4,
    '#maxlength' => 3,
    '#field_suffix' => '%',
  );

  // Make it a settings form.
  $form = system_settings_form($form);


  // Add example of highlighting.
  $text = 'Lorem ipsum';
  $lorem_ipsum = explode(' ', 'lorem ipsum dolor sit amet consectetuer adipiscing elit integer viverra diam suspendisse aliquam ligula ut justo cras sodales tortor in');
  $result = db_query_range('SELECT uid, name FROM {users} WHERE name <> "" ORDER BY RAND()', 0, 20);
  while ($user = db_fetch_object($result)) {
    $text .= ' '. $user->name;
    foreach (array_rand($lorem_ipsum, rand(2, 5)) as $i) {
      $text .= ' '. $lorem_ipsum[$i];
    }
  }
  $text .= '.';
  $form['username_highlighter_example'] = array(
    '#type' => 'item',
    '#title' => t('Example'),
    '#value' => '<p>'. username_highlighter_filter('process', 0, -1, $text) .'</p>',
  );

  return $form;
}


/**
 * Validation handler for username_highlighter_admin form.
 */
function username_highlighter_admin_validate($form, &$form_state) {
  $sat = $form_state['values']['username_highlighter_color_saturation'];
  if (!is_numeric($sat) || $sat < 0.0 || $sat > 100.0) {
    form_set_error('username_highlighter_color_saturation', t('Color saturation should be between 0% and 100%'));
  }
  $val = $form_state['values']['username_highlighter_color_value'];
  if (!is_numeric($val) || $val < 0.0 || $val > 100.0) {
    form_set_error('username_highlighter_color_value', t('Color value should be between 0% and 100%'));
  }
}


/**
 * Helper function for mapping user id to a color.
 */
function _username_highlighter_uid2color($uid) {
  $hue = ($uid * 67) % 360;
  $sat = 0.01 * (float)(variable_get('username_highlighter_color_saturation', 50));
  $val = 0.01 * (float)(variable_get('username_highlighter_color_value', 100));
  return _username_highlighter_hsv2rgbhexcode($hue, $sat, $val);
}


/**
 * Helper function for fetching the user name replacements.
 */
function _username_highlighter_username_map() {
  static $username_map = NULL;
  if (!$username_map) {
    $result = db_query('SELECT uid, name FROM {users} WHERE name <> ""');
    while ($user = db_fetch_object($result)) {
      $color = _username_highlighter_uid2color($user->uid);
      $username_map['/\\b'. $user->name .'\\b/'] =
        l($user->name, 'user/' . $user->uid,
          array('attributes' => array(
            'class' => 'user-name-highlighter user-name-highlighter-uid-'. $user->uid,
            'style' => 'background-color:'. $color .';')
          )
        );
    }
  }
  return $username_map;
}


/**
 * Helper function for HSV to RGB hex code conversion.
 * @param $h the hue, in range [0-360]
 * @param $s the saturation, in range [0-1]
 * @param $v the value, in range [0-1]
 */
function _username_highlighter_hsv2rgbhexcode($h, $s, $v) {
  // Map hue to the piecewise linear R, G, B curves in the range [0-1] first.
  $r = min(1.0, max(0.0, abs($h-180.0)/60.0 - 1));
  $g = min(1.0, max(0.0, 2 - abs($h-120.0)/60.0));
  $b = min(1.0, max(0.0, 2 - abs($h-240.0)/60.0));
  // Rescale dynamic range, considering value and saturation.
  $r = min(255, max(0, (int)(256 * ($r*$v + (1.0-$r)*(1.0-$s)*$v))));
  $g = min(255, max(0, (int)(256 * ($g*$v + (1.0-$g)*(1.0-$s)*$v))));
  $b = min(255, max(0, (int)(256 * ($b*$v + (1.0-$b)*(1.0-$s)*$v))));
  return sprintf('#%02X%02X%02X', $r, $g, $b);
}
